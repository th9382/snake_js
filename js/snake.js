(function () {
  if (typeof SG === 'undefined') {
    window.SG = {};
  }

  var Coord = SG.Coord = function (row, col) {
    this.row = row;
    this.col = col;
  };

  Coord.prototype.equals = function (coord2) {
    return (this.row == coord2.row) && (this.col == coord2.col);
  };

  Coord.prototype.isOpposite = function (coord2) {
    return (this.row == (-1 * coord2.row)) && (this.col == (-1 * coord2.col));
  };

  Coord.prototype.plus = function (coord2) {
    return new Coord(this.row + coord2.row, this.col + coord2.col);
  };

  var Apple = SG.Apple = function (board) {
    this.board = board;
    this.generate();
  };

  Apple.prototype.generate = function () {
    var row, column;

    do {
      row = Math.floor( Math.random() * this.board.dim );
      column = Math.floor( Math.random() * this.board.dim );
    } while ( this.board.snake.isOccupying([row, column]));

    this.position = new Coord(row, column);
  };

  var Snake = SG.Snake = function (board) {
    this.dir = "S";
    this.board = board;
    this.speed = 200;
    this.alive = true;
    this.turning = false;

    var center = new Coord(Math.floor(board.dim/2), Math.floor(board.dim/2));
    this.segments = [center];
  };

  Snake.DIFFS = {
    "N": new Coord(-1, 0),
    "E": new Coord(0, 1),
    "S": new Coord(1, 0),
    "W": new Coord(0, -1)
  };

  Snake.prototype.increaseSpeed = function() {
    this.speed *= 0.98;
  };

  Snake.prototype.resetSpeed = function() {
    this.speed = 200;
  };

  Snake.prototype.eatApple = function () {
    if (this.head().equals(this.board.apple.position)) {
      return true;
    } else {
      return false;
    }
  };

  Snake.prototype.isOccupying = function (array) {
    var result = false;
    this.segments.forEach(function (segment) {
      if (segment.row === array[0] && segment.col === array[1]) {
        result = true;
        return result;
      }
    });
    return result;
  };

  Snake.prototype.head = function () {
    return this.segments[this.segments.length - 1];
  };

  Snake.prototype.isValid = function () {
    var head = this.head();

    if (!this.board.validPosition(head)) {
      return false;
    }

    for (var i = 0; i < this.segments.length - 1; i++) {
      if (this.segments[i].equals(head)) {
        return false;
      }
    }
    return true;
  };

  Snake.prototype.move = function () {
    this.turning = false;

    this.segments.push(this.head().plus(Snake.DIFFS[this.dir]));

    if (this.eatApple()) {
      // improve transition between snake and apple
      $(".apple").removeClass().addClass("snake");
      this.board.apple.generate();
    } else {
      this.segments.shift();
    }

    if (!this.isValid()) {
      this.alive = false;
      this.segments.pop();
    }
  };

  Snake.prototype.turn = function (dir) {
    // avoid turning directly back on yourself
    if (Snake.DIFFS[this.dir].isOpposite(Snake.DIFFS[dir]) ||
      this.turning) {
      return;
    } else {
      this.turning = true;
      this.dir = dir;
    }
  };

  var Board = SG.Board = function (dim) {
    this.dim = dim;
    this.snake = new Snake(this);
    this.apple = new Apple(this);
  };

  Board.prototype.validPosition = function (coord) {
    return (coord.row >= 0) && (coord.row < this.dim) &&
      (coord.col >= 0) && (coord.col < this.dim);
  };
})();
